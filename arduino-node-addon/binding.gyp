{
  "targets": [
    {
      "target_name": "arduino-node-addon",
      "sources": [ 
        "Adafruit_GFX.cc",
        "Adafruit_NeoMatrix.cc",
        "Adafruit_NeoPixel.cc", 
        "Print.cc",
        "hello.cc"      
      ],
      "include_dirs": [
        "<!(node -e \"require('nan')\")"
      ],
      "cflags": [ "-std=c++11" ]
    },    
  ]
}
