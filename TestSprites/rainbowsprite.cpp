#include "rainbowsprite.h"


//TODO: sprite rainbow, basé sur le code suivant:

/*
// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< matrix.numPixels(); i++) {
      matrix.setPixelColor(i, Wheel(((i * 256 / matrix.numPixels()) + j) & 255));
    }
    matrix.show();
    delay(wait);
  }
}
*/

RainbowSprite::RainbowSprite(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps) : Sprite(width, height, x, y, fps) {
}

bool RainbowSprite::draw() {
    for(uint8_t y=0; y< HEIGHT; y++) {
        for(uint8_t x=0; x< WIDTH; x++) {
            int i = (y*WIDTH)+x;
            drawPixel(x, y,  Wheel(((i * 256 / ((WIDTH*HEIGHT) + _progress) & 255))));
        }
    }
    _progress++;
    return true;
}


// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t RainbowSprite::Wheel(uint8_t WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}