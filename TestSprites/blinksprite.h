#ifndef Sprite_Blink_h
#define Sprite_Blink_h

#include <sprite.h>

class BlinkSprite : public Sprite {
  public:
    BlinkSprite(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps);
    virtual bool draw();
};

#endif
