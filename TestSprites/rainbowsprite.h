#ifndef Sprite_Rainbow_h
#define Sprite_Rainbow_h

#include <sprite.h>

class RainbowSprite : public Sprite {
  public:
    RainbowSprite(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps);
    uint32_t Wheel(uint8_t WheelPos);
    virtual bool draw();
  private:
    uint8_t _progress=0; //0-255
};

#endif
