#ifndef _SPRITE_H
#define _SPRITE_H

#include <Adafruit_GFX.h>


class NewSprite : public Adafruit_GFX {
	public:
		NewSprite(uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps);
		~NewSprite(void);

		uint16_t Color(uint8_t r, uint8_t g, uint8_t b);

		uint16_t *getBuffer(void);

		uint8_t getState();
		void setState(uint8_t state);
		void setFPS(uint8_t fps);
		
		uint8_t getHeight();
		uint8_t getWidth();
		int16_t getLeft();
		int16_t getTop();

		void drawPixel(int16_t x, int16_t y, uint16_t color);
		//void fillScreen(uint16_t color);
		bool needScreenUpdate(unsigned long timer);
		virtual bool draw() = 0;

	protected:
		uint16_t *_buffer;
		uint8_t _fps=0;
		int16_t _x;
		int16_t _y;
		int16_t _w;
		int16_t _h;
		uint8_t _state=1;
		unsigned long _lastUpdate=-1000;
};

#endif