# debug classe sprite

probleme: overflow du texte qd dessiné a cheval a gauche de l'ecran
-tentative de fix en agrandissant le buffer mais ca debord lorsqu'on use le drawBitmap sur la matrice

# affichage

trois zones a l'ecran:

premiere, rouge = affichage directement sur matrice, pas de bug observé

deuxieme, bleu = classe sprite classique, bug d'artefact sur la droite lorsque qu'un caractere est dessiné a cheval a gauche de l'ecran

troisieme, vert = classe sprite avec les tentative de fix et la mise en evidence du depassement de buffer

![preview](https://i.imgur.com/eI2Dhr4.png)

# compile
    
    cd Hazurl
    ./build.sh

# run

    ./hazurl