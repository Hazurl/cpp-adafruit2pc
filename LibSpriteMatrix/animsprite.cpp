#include "animsprite.h"

AnimSprite::AnimSprite(uint16_t* spritesheetBitmap, uint16_t spritesheetIndex, uint8_t frameCount, bool loop, uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps) : Sprite(width, height, x, y, fps) {
    setState(STATES::DISABLED);
    _spritesheetBitmap=spritesheetBitmap;
    _spritesheetIndex=spritesheetIndex;
    _frameCount=frameCount;
    _loop=loop;
}

void AnimSprite::play() {
    _frameIndex=0;
    setState(STATES::PLAYING);
} 

void AnimSprite::pause() {
    _frameIndex=0;
    setState(STATES::PAUSED);
}

void AnimSprite::stop() {
    _frameIndex=0;
    setState(STATES::STOPPED);
}

bool AnimSprite::draw() {

    uint8_t currentState=getState();
    bool needUpdate=false;

    if(currentState!=_previousState) {
        if(currentState==STATES::DISABLED) {
            //effacer le buffer si on passe a l'etat disabled?
            fillScreen(0);
            _previousState=currentState;
            return true;
        } 
    }
    
    if(currentState==STATES::PLAYING) {

        //TODO: draw la nouvelle frame de la spritesheet ds le buffer du sprite
        drawRGBBitmap(0,0, _spritesheetBitmap, WIDTH,HEIGHT);

        needUpdate=true;
        
        //incremente la frame suivante    
        _frameIndex++;
        if(_frameIndex>=_frameCount){
            if(_loop){
                _frameIndex=0;
            } else {
                stop();
            }
        }
        
    } else {
        
    }
   
   _previousState=currentState;
   return needUpdate;
}
