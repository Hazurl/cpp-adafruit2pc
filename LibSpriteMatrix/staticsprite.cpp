#include "staticsprite.h"

StaticSprite::StaticSprite(const uint16_t spritesheetBitmap[], uint16_t spritesheetIndex, uint8_t width, uint8_t height, int16_t x, int16_t y) : Sprite(width, height, x, y, 2) {
    setState(STATES::ENABLED);
    _spritesheetBitmap=spritesheetBitmap;
    _spritesheetIndex=spritesheetIndex;
}

bool StaticSprite::draw() {

    uint8_t currentState=getState();

    //si state inchange = on fait rien
    if(currentState==_previousState) {
        return false;
    }
        
    //le state a change. regarde le nouvel etat!
    if(currentState==STATES::DISABLED) {
        //effacer le buffer si on passe a l'etat disabled?
        fillScreen(0);
    } else if(currentState==STATES::ENABLED) {
        //TODO: draw la nouvelle frame de la spritesheet ds le buffer du sprite
        //drawRGBBitmap(0,0, _spritesheetBitmap, WIDTH, HEIGHT);
        
        for(uint16_t y=0; y<HEIGHT; y++) {
            for(uint16_t x=0; x<WIDTH; x++) {
                uint16_t addr=(y*WIDTH)+x;
                drawPixel(x,y, pgm_read_word(&_spritesheetBitmap[addr]));
            }
        }
        
        
    }
   
   _previousState=currentState;
   return true;
}
