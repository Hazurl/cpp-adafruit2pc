#ifndef Sprite_Static_h
#define Sprite_Static_h

#include "sprite.h"

class StaticSprite : public Sprite
{
    public:
        StaticSprite(const uint16_t spritesheetBitmap[], uint16_t spritesheetIndex, uint8_t width, uint8_t height, int16_t x, int16_t y);
        ~StaticSprite(void);

        enum STATES {   
            DISABLED,
            ENABLED
        };

        bool draw();
        uint16_t* getBitmap();

    private:
        const uint16_t*     _spritesheetBitmap=0;
        uint8_t             _spritesheetIndex=0;
        uint8_t             _previousState=STATES::DISABLED;
};

#endif
