#ifndef Sprite_Anim_h
#define Sprite_Anim_h

#include "sprite.h"

class AnimSprite : public Sprite
{
    public:
        AnimSprite(uint16_t* spritesheetBitmap, uint16_t spritesheetIndex, uint8_t frameCount, bool loop, uint8_t width, uint8_t height, int16_t x, int16_t y, uint8_t fps);
        ~AnimSprite(void);
        
        void play();
        void pause();
        void stop();
        bool draw();

        enum STATES {
            DISABLED,
            STOPPED,
            PAUSED,
            PLAYING
        };

    protected:
        uint8_t     _frameIndex=0;
        uint8_t     _frameCount=0;
        bool        _loop=true;

    private:
        uint16_t*   _spritesheetBitmap=0;
        uint8_t     _spritesheetIndex=0;
        uint8_t     _previousState=0;
};

#endif
