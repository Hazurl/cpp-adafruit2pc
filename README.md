
# Libs Adafruit: GFX , Neomatrix, et NeoPixel, portées sur PC !

# Build 

## Setup linux (et bash ?)

    sudo apt-get install build-essentials

## Compilation

    cd TestProg
    ./build.sh

## Lancement prog test

    ./matrixtest

# Ou est le code ?

- Le code qui gere l'affichage, est la fonction void Adafruit_NeoMatrix::show(void) dans le fichier "Adafruit_NeoMatrix_Ported/Adafruit_NeoMatrix.cpp"
- Le code d'exemple est dans TestProg/matrixtest.cpp

